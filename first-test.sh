CURRENTWORKSPACE=$(i3-msg -t get_workspaces | sed 's/},{/\n/g' | grep "focused\":true" | sed 's/,/\n/g' | grep num | cut -d ":" -f2)



echo $CURRENTWORKSPACE

moveUp () {
	if [ $CURRENTWORKSPACE -gt 3 ]
	then
		MOVETO=$(($CURRENTWORKSPACE - 3))
		i3-msg workspace number $MOVETO
	else
		MOVETO=$(($CURRENTWORKSPACE + 6))
		i3-msg workspace number $MOVETO
	fi	
}


moveDown () {
	if [ $CURRENTWORKSPACE -lt 7 ]
	then
		MOVETO=$(($CURRENTWORKSPACE + 3))
		i3-msg workspace number $MOVETO
	else
		MOVETO=$(($CURRENTWORKSPACE - 6))
		i3-msg workspace number $MOVETO
	fi	
}



moveLeft () {
	if [ $CURRENTWORKSPACE != 1 ] && [ $CURRENTWORKSPACE != 4 ] && [ $CURRENTWORKSPACE != 7 ]
	then
		MOVETO=$(($CURRENTWORKSPACE - 1))
		i3-msg workspace number $MOVETO
	else
		MOVETO=$(($CURRENTWORKSPACE + 2 ))
		i3-msg workspace number $MOVETO
	fi	
}

moveRight () {
	if [ $CURRENTWORKSPACE != 3 ] && [ $CURRENTWORKSPACE != 6 ] && [ $CURRENTWORKSPACE != 9 ]
	then
		MOVETO=$(($CURRENTWORKSPACE + 1))
		i3-msg workspace number $MOVETO
	else
		MOVETO=$(($CURRENTWORKSPACE - 2 ))
		i3-msg workspace number $MOVETO
	fi	
}


case $1 in
	Right) moveRight ;;
	Left) moveLeft ;;
	Up) moveUp ;;
	Down) moveDown ;;
esac


: '

| 1 | 2 | 3 |
| 4 | 5 | 6 |
| 7 | 8 | 9 |

'
