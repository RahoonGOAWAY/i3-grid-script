Welcome to whateber this is

## USAGE: 

Command: `./first-test.sh <direction>`.

This will move your workspace in the direction specified

Directions are: Right Left Up Down

## The grid
Looks something like this:

```
	| 1 | 2 | 3 |
	| 4 | 5 | 6 |
	| 7 | 8 | 9 |
```

## Hotkeys
I guess you will have to add the keybindings to whatever hotkey deamon you use

mine in sxhkd look like this:

```
mod1 + Left
	. ~/Scripts/i3-grid/first-test.sh Left

mod1 + Right
	. ~/Scripts/i3-grid/first-test.sh Right

mod1 + Up
	. ~/Scripts/i3-grid/first-test.sh Up

mod1 + Down
	. ~/Scripts/i3-grid/first-test.sh Down
```


if you know a better way to do this, please do tell
